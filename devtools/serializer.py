import os, sys, json, collections, re
from lxml import etree
from enum import IntEnum, auto
from typing import List, Optional, Any, Dict, Set, Pattern
from buildtools import os_utils, log
from buildtools.indentation import IndentWriter
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from mkpackage import make_package
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap, CommentedSeq
yaml=YAML()
REG_FIX_IMPORTS: Pattern = re.compile(r'([a-z\.]+):\s+- ([a-z\.]+)', re.MULTILINE|re.IGNORECASE)

class EVarType(IntEnum):
    STR = auto()
    INT = auto()
    FLOAT = auto()
    BOOL = auto()
    OBJECT = auto()
    LIST = auto()
    RAW = auto()
    ENUM = auto()

class ESourceType(IntEnum):
    ATTRIBUTE = auto()
    ELEMENT = auto()

KEYWORDS = [
    'and',
    'as',
    'assert',
    'async',
    'await',
    'break',
    'class',
    'continue',
    'def',
    'del',
    'elif',
    'else',
    'except',
    'finally',
    'for',
    'from',
    'global',
    'if',
    'import',
    'in',
    'is',
    'lambda',
    'nonlocal',
    'not',
    'or',
    'pass',
    'raise',
    'return',
    'try',
    'while',
    'with',
    'yield',
]
class Variable(object):
    TYPE = None
    PYTYPE = ''
    DEFAULT_PY_VALUE = None
    @staticmethod
    def FromXMLAttribute(cls, k, v) -> 'Variable':
        # Everything is a string.
        o: Variable = None
        if isinstance(v, str):
            if v.isdigit():
                o = IntVar(cls)
            elif v.isdecimal():
                o = FloatVar(cls)
            elif v in ('true', 'false'):
                o = BoolVar(cls)
            else:
                o = StrVar(cls)
        elif isinstance(v, int):
            o = IntVar(cls)
        elif isinstance(v, float):
            o = FloatVar(cls)
        elif isinstance(v, bool):
            o = BoolVar(cls)
        elif isinstance(v, list):
            o = ListVar(cls)
        else:
            o = ObjectVar()
        o.attr_id = k
        o.py_id = k.lower()
        if o.py_id in KEYWORDS:
            o.py_id += '_'
        return o
    @staticmethod
    def FromYAML(cls, k, v, source: ESourceType) -> 'Variable':
        vtype: str = v.get('type')
        o: Variable = None
        if 'type' not in v:
            if v.get('ignore', False):
                vtype = 'raw'
                v['type'] = vtype
        if vtype == 'str':
            o = StrVar(cls)
        elif vtype == 'int':
            o = IntVar(cls)
        elif vtype == 'float':
            o = FloatVar(cls)
        elif vtype == 'bool':
            o = BoolVar(cls)
        elif vtype == 'list':
            if source == ESourceType.ATTRIBUTE:
                o = ListVar(cls)
            else:
                o = ListElement(cls)
        elif vtype == 'raw':
            o = RawVar(cls)
        elif vtype == 'enum':
            o = EnumVar(cls)
        elif vtype == 'object':
            o = ObjectVar(cls)
        else:
            print(f'type == {vtype!r}')
        o.deserialize(v)
        #print(k, type(v))
        return o

    def __init__(self, apiclass: 'APIClass'):
        self.apiclass: 'APIClass' = apiclass
        self.attr_id: str = ''
        self.py_id: str = ''
        self.default: Any = self.DEFAULT_PY_VALUE
        self.required: bool = True
        self.ignore: bool = False

    def genInitCode(self, w) -> None:
        c = '#' if self.ignore else ''
        if self.default is None:
            w.writeline(f"{c}self.{self.py_id}: Optional[{self.PYTYPE}] = None")
        else:
            w.writeline(f"{c}self.{self.py_id}: {self.PYTYPE} = {self.default!r}")

    def getDeserializeArgs(self) -> List[str]:
        args: List[str] = [f'{self.attr_id!r}']
        if not self.required:
            args += [f'default={self.default!r}']
        args += [f'required={self.required!r}']
        return args

    def genDeserializeCode(self, method: str, w, dataname: Optional[str]) -> None:
        args: List[str] = self.getDeserializeArgs()
        sargs = ', '.join(args)
        c = '#' if self.ignore else ''
        w.writeline(f"{c}self.{self.py_id} = {method}({sargs})")

    def genSerializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        if self.required:
            w.writeline(f"{c}{dataname}[{self.attr_id!r}] = str(self.{self.py_id})")
        else:
            with w.writeline(f'{c}if self.{self.py_id} != {self.default!r}:'):
                w.writeline(f"{c}{dataname}[{self.attr_id!r}] = str(self.{self.py_id})")

    def serialize(self) -> dict:
        o = CommentedMap()
        if self.py_id == self.attr_id:
            o['id'] = self.py_id
        else:
            o['py_id'] = self.py_id
            o['attr_id'] = self.attr_id
        if self.ignore:
            o['ignore'] = self.ignore
        if self.TYPE != EVarType.RAW:
            o['type'] = self.TYPE.name.lower()
        if self.default != self.DEFAULT_PY_VALUE:
            o['default'] = self.default
        if not self.required:
            o['required'] = self.required
        return o

    def deserialize(self, data: dict) -> None:
        self.ignore = data.get('ignore', self.ignore)
        self.default = data.get('default', self.default)
        if 'id' in data:
            self.attr_id = data['id']
            self.py_id = data['id']
        else:
            self.attr_id = data['attr_id']
            self.py_id = data['py_id']

        if self.default is None:
            self.apiclass.addImports({'typing':['Optional']})

        if self.default is None:
            self.required = False
        self.required = data.get('required', self.required)

class StrVar(Variable):
    TYPE = EVarType.STR
    PYTYPE = 'str'
    DEFAULT_PY_VALUE = ''
    def genInitCode(self, w) -> None:
        super().genInitCode(w)
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        super().genDeserializeCode('self._getStr', w, dataname)

class IntVar(Variable):
    TYPE = EVarType.INT
    PYTYPE = 'int'
    DEFAULT_PY_VALUE = -1
    def genInitCode(self, w) -> None:
        super().genInitCode(w)
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        super().genDeserializeCode('self._getInt', w, dataname)
class FloatVar(Variable):
    TYPE = EVarType.FLOAT
    PYTYPE = 'float'
    DEFAULT_PY_VALUE = -1.0
    def genInitCode(self, w) -> None:
        super().genInitCode(w)
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        super().genDeserializeCode('self._getFloat', w, dataname)
class BoolVar(Variable):
    TYPE = EVarType.BOOL
    PYTYPE = 'bool'
    DEFAULT_PY_VALUE = False
    def genInitCode(self, w) -> None:
        super().genInitCode(w)
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        super().genDeserializeCode('self._getBool', w, dataname)
class RawVar(Variable):
    TYPE = EVarType.RAW
    PYTYPE = 'Any'
    DEFAULT_PY_VALUE = None
    def __init__(self, cls):
        super().__init__(cls)
        self.typeHint: str = self.PYTYPE
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.typeHint = data.get('type-hint', 'Any')
        if self.typeHint == 'Any':
            self.apiclass.addImports({'typing':['Any']})
    def serialize(self) -> dict:
        o = super().serialize()
        o['type-hint'] = self.typeHint
        return o
    def genInitCode(self, w) -> None:
        c = '#' if self.ignore else ''
        if self.default is None:
            w.writeline(f"{c}self.{self.py_id}: Optional[{self.typeHint}] = None")
        else:
            w.writeline(f"{c}self.{self.py_id}: {self.typeHint} = {self.default!r}")
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        super().genDeserializeCode('self._getRawValue', w, dataname)
class EEnumValueType(IntEnum):
    NAME = auto()
    VALUE = auto()
class EnumVar(Variable):
    TYPE = EVarType.ENUM
    def __init__(self, cls):
        super().__init__(cls)
        self.enumType: str = 'ENUM_TYPE'
        self.valueType: EEnumValueType = EEnumValueType.NAME
    def serialize(self) -> dict:
        o = super().serialize()
        o['enum-type'] = self.enumType
        o['value-type'] = self.valueType.name.lower()
        return o
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.enumType = data['enum-type']
        self.valueType = EEnumValueType[data['value-type'].upper()]

    def genInitCode(self, w) -> None:
        c = '#' if self.ignore else ''
        w.writeline(f"{c}self.{self.py_id}: {self.enumType} = {self.enumType}.{self.default}")
    def genDeserializeCode(self, method: str, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        if self.valueType == EEnumValueType.NAME:
            w.writeline(f"{c}self.{self.py_id} = {self.enumType}[self._getRawValue({self.attr_id!r}).upper()]")
        else:
            w.writeline(f"{c}self.{self.py_id} = {self.enumType}(self._getRawValue({self.attr_id!r}))")
    def genSerializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        if self.valueType == EEnumValueType.NAME:
            w.writeline(f"{c}{dataname}[{self.attr_id!r}] = self.{self.py_id}.name.lower()")
        else:
            w.writeline(f"{c}{dataname}[{self.attr_id!r}] = self.{self.py_id}.value")

class ObjectVar(Variable):
    TYPE = EVarType.OBJECT
    def __init__(self, cls):
        super().__init__(cls)
        self.factoryMethod: str = 'FACTORY_METHOD'
        self.objType: str = 'TYPE'
        self.multiple: bool = False

    def serialize(self) -> dict:
        o = super().serialize()
        o['factory-method'] = self.factoryMethod
        o['obj-type'] = self.objType
        o['multiple'] = self.multiple
        return o
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.factoryMethod = data['factory-method']
        self.objType = data['obj-type']
        self.multiple = data.get('multiple', False)

        if not self.multiple:
            self.apiclass.addImports({'typing':['Optional']})
        else:
            self.apiclass.addImports({'typing':['List']})
        if not self.multiple and self.required:
            self.apiclass.addImports({'sys':None})
    def genInitCode(self, w) -> None:
        c = '#' if self.ignore else ''
        typehint = self.objType
        default = f'{self.factoryMethod}()'
        if self.multiple:
            typehint = f'List[{typehint}]'
            default = '[]'
        elif not self.required:
            typehint = f'Optional[{typehint}]'
            default = 'None'
        w.writeline(f"{c}self.{self.py_id}: {typehint} = {default}")
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        if self.multiple:
            w.writeline(f'{c}self.{self.py_id} = []')
            with w.writeline(f'{c}for el in {dataname}.findall({self.attr_id!r}):'):
                w.writeline(f'{c}o = {self.factoryMethod}()')
                w.writeline(f'{c}o.deserialize(el)')
                w.writeline(f'{c}self.{self.py_id} += [o]')
        elif not self.required:
            w.writeline(f'{c}e: Optional[etree.Element] = {dataname}.find({self.attr_id!r})')
            w.writeline(f'{c}self.{self.py_id} = None')
            with w.writeline(f'{c}if e is not None:'):
                w.writeline(f"{c}self.{self.py_id} = {self.factoryMethod}()")
                w.writeline(f"{c}self.{self.py_id}.deserialize(e)")
        else:
            w.writeline(f'{c}e: Optional[etree.Element] = {dataname}.find({self.attr_id!r})')
            w.writeline(f'{c}self.{self.py_id} = None')
            with w.writeline(f'{c}if e is None:'):
                w.writeline(f"{c}print('Element <{self.attr_id}> cannot be found in element <{self.apiclass.tag}>!')")
                w.writeline(f"{c}print('This likely means the save is corrupt. :(')")
                w.writeline(f"{c}sys.exit(1)")
            w.writeline(f"{c}self.{self.py_id} = {self.factoryMethod}()")
            w.writeline(f"{c}self.{self.py_id}.deserialize(e)")
    def genSerializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        if self.multiple:
            with w.writeline(f"{c}for e in self.{self.py_id}:"):
                w.writeline(f"{c}{dataname}.append(e.serialize())")
        elif not self.required:
            with w.writeline(f"{c}if self.{self.py_id} is not None:"):
                w.writeline(f"{c}{dataname}.append(self.{self.py_id}.serialize())")
        else:
            w.writeline(f"{c}{dataname}.append(self.{self.py_id}.serialize())")

class ListVar(Variable):
    TYPE = EVarType.LIST
    def __init__(self, cls):
        super().__init__(cls)
        self.apiclass.addImports({'typing': ['List']})
        self.delimiter: str = ','
        self.obj_type: str = 'str'

    def serialize(self) -> dict:
        o = super().serialize()
        e = CommentedMap()
        o['delimiter'] = self.delimiter
        o['obj-type'] = self.obj_type
        return o
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.delimiter = data['delimiter']
        self.obj_type = data['obj-type']
    def genInitCode(self, w) -> None:
        c = '#' if self.ignore else ''
        w.writeline(f"{c}self.{self.py_id}: List[{self.obj_type}] = []")
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        w.writeline(f"{c}self.{self.py_id} = [{self.obj_type}(x.strip()) for x in {dataname}.get({self.attr_id!r}, '').split({self.delimiter!r})]")
    def genSerializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        if self.required:
            w.writeline(f"{c}{dataname}[{self.attr_id!r}] = {self.delimiter!r}.join([str(x) for x in self.{self.py_id}])")
        else:
            with w.writeline(f"{c}if len(self.{self.py_id}) > 0:"):
                w.writeline(f"{c}{dataname}[{self.attr_id!r}] = {self.delimiter!r}.join([str(x) for x in self.{self.py_id}])")

class ListElement(Variable):
    TYPE = EVarType.LIST
    def __init__(self, cls):
        super().__init__(cls)
        self.apiclass.addImports({'typing': ['List']})
        self.entry_tag: str = 'ENTRY'
        self.entry_attr: Optional[str] = 'ATTRIBUTE'
        self.obj_type: str = 'str'

    def serialize(self) -> dict:
        o = super().serialize()
        e = CommentedMap()
        e['tag'] = self.entry_tag
        if self.entry_attr is not None:
            e['attribute'] = self.entry_attr
        o['entry'] = e
        o['obj-type'] = self.obj_type
        return o
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        e = data['entry']
        self.entry_tag = e['tag']
        self.entry_attr = e.get('attribute', None)
        self.obj_type = data['obj-type']
    def genInitCode(self, w) -> None:
        c = '#' if self.ignore else ''
        w.writeline(f"{c}self.{self.py_id}: List[{self.obj_type}] = []")
    def genDeserializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        w.writeline(f"{c}self.{self.py_id} = []")
        with w.writeline(f"{c}for a in {dataname}.find({self.attr_id!r}):"):
            access_method = '.text' if self.entry_attr is None else f".attrib[{self.entry_attr!r}]"
            w.writeline(f"{c}self.{self.py_id}.append({self.obj_type}(a{access_method}))")
    def genSerializeCode(self, w, dataname: Optional[str]) -> None:
        c = '#' if self.ignore else ''
        w.writeline(f"{c}e = etree.Element({self.attr_id!r})")
        with w.writeline(f"{c}for value in self.{self.py_id}:"):
            w.writeline(f"{c}inner = etree.Element({self.entry_tag!r})")
            if self.obj_type is None:
                w.writeline(f'{c}inner.text = str(value)')
            else:
                w.writeline(f'{c}inner[{self.entry_attr!r}] = str(value)')
            w.writeline(f'{c}e.append(inner)')
        w.writeline(f'{c}{dataname}.append(e)')

class APIClass(object):
    def __init__(self):
        self.tag: str = ''
        self.package: str = ''
        self.name: str = ''
        self.attributes: Dict[str, Variable] = {}
        self.children:   Dict[str, Variable] = {}
        self.imports: Dict[str, Optional[Set[str]]] = {}
        self.addImports({
            #'typing': ['List', 'Optional'],
            'lxml': ['etree'],
            'barotweak.model.baseserializable': ['BaseSerializable'],
        })

    @classmethod
    def FromXMLElement(cls, e: etree.Element) -> 'APIClass':
        c = cls()
        c.name = e.tag
        c.tag = e.tag
        for k, v in e.attrib.items():
            c.attributes[k] = Variable.FromXMLAttribute(c, k, v)
        for child in e:
            k = child.tag.lower()
            if k not in c.children:
                c.children[k] = APIClass.FromXMLElement(child).serializeAsObjectVar()
            else:
                c.children[k].multiple = True
        return c

    def addImports(self, newimps: Dict[str, Optional[List[str]]]) -> None:
        for pkg, imps in newimps.items():
            if pkg not in self.imports or imps is None:
                self.imports[pkg] = set(imps) if imps is not None else None
            else:
                self.imports[pkg] = self.imports[pkg].union(set(imps))

    def serializeAsObjectVar(self) -> dict:
        o = ObjectVar(self)
        o.attr_id = self.tag
        o.py_id = self.tag.lower()
        if o.py_id in KEYWORDS:
            o.py_id += '_'
        o.factory_method = self.name
        o.type_obj = self.name
        return o

    def serialize(self) -> dict:
        o = CommentedMap()
        o['package'] = self.package
        o['name'] = self.name
        o['tag'] = self.tag
        o['imports'] = {k: list(v) for k, v in self.imports.items()}
        o['attributes'] = {k: v.serialize() for k, v in self.attributes.items()}
        o['children'] = {k: v.serialize() for k, v in self.children.items()}
        return o

    def deserialize(self, data: dict) -> None:
        self.name = data['name']
        self.package = data['package']
        self.tag = data['tag']
        self.addImports(data['imports'])
        self.attributes = {k: Variable.FromYAML(self, k, v, ESourceType.ATTRIBUTE) for k,v in data['attributes'].items()}
        self.children = {k: Variable.FromYAML(self, k, v, ESourceType.ELEMENT) for k,v in data['children'].items()}

    def genCode(self, w):
        for pkg in sorted(self.imports.keys()):
            clsname = self.imports[pkg]
            if clsname is not None:
                clsnames = ', '.join(sorted(clsname))
                w.writeline(f'from {pkg} import {clsnames}')
            else:
                w.writeline(f'import {pkg}')
        with w.writeline(f'class {self.name}(BaseSerializable):'):
            w.writeline(f'TAG = {self.tag!r}')
            with w.writeline(f'def __init__(self):'):
                w.writeline('super().__init__()')
                for v in self.attributes.values():
                    v.genInitCode(w)
                for v in self.children.values():
                    v.genInitCode(w)
            with w.writeline(f'def serialize(self) -> etree.Element:'):
                w.writeline('o=super().serialize()')
                for v in self.attributes.values():
                    v.genSerializeCode(w, 'o')
                for v in self.children.values():
                    v.genSerializeCode(w, 'o')
                w.writeline('return o')
            with w.writeline(f'def deserialize(self, data: etree.Element) -> None:'):
                w.writeline('super().deserialize(data)')
                for v in self.attributes.values():
                    v.genDeserializeCode(w,'data')
                for v in self.children.values():
                    v.genDeserializeCode(w,'data')
                #w.writeline('self.finishDeserialize(data)')

class GenerateModel(SingleBuildTarget):
    BT_LABEL = 'GEN MODEL'
    def __init__(self, pyfile: str, ymlfile: str, dependencies: List[str] = []):
        self.pyfile: str = pyfile
        self.ymlfile: str = ymlfile
        super().__init__(target=self.pyfile, files=[self.ymlfile, os.path.abspath(__file__)], dependencies=dependencies)
    def build(self):
        c = APIClass()
        with open(self.ymlfile, 'r') as f:
            c.deserialize(yaml.load(f))
        with open(self.pyfile, 'w') as f:
            w = IndentWriter(f, indent_chars='    ')
            c.genCode(w)

def main():
    import argparse
    argp = argparse.ArgumentParser()
    subp = argp.add_subparsers(dest='command')
    import_argp = subp.add_parser('import')
    import_argp.add_argument('infile')
    import_argp.add_argument('classname')
    import_argp.add_argument('xpath', type=str, help='XPath to object.  Example: //Gamesession')

    import_argp = subp.add_parser('gencode')
    import_argp.add_argument('--clean', action='store_true', default=False, help='Cleans everything.')
    import_argp.add_argument('--no-colors', action='store_true', default=False, help='Disables colors.')
    import_argp.add_argument('--rebuild', action='store_true', default=False, help='Clean rebuild of project.')
    import_argp.add_argument('--show-commands', action='store_true', default=False, help='Echoes the line used to execute commands. (echo=True in os_utils.cmd())')
    import_argp.add_argument('--verbose', action='store_true', default=False, help='Show hidden buildsteps.')

    args = argp.parse_args()

    c = APIClass()
    if args.command == 'import':
        origdata = etree.parse(args.infile)
        element = origdata.xpath(args.xpath)[0]
        if element is None:
            log.critical('Unable to find anything using that XPath.')
            sys.exit(1)
        c = APIClass.FromXMLElement(element)
        c.package = 'barotweak.model._raw'
        c.name = f'Base{args.classname}'
        suffix = '.new' if os.path.isfile(os.path.join('data', 'model', f'{args.classname}.yml')) else ''
        with open(os.path.join('data', 'model', f'{args.classname}.yml'+suffix), 'w') as f:
            yaml.dump(c.serialize(), f)
        data = ''
        with open(os.path.join('data', 'model', f'{args.classname}.yml'+suffix), 'r') as f:
            data = f.read()
        data = REG_FIX_IMPORTS.sub(r'\1: [\2]', data)
        with open(os.path.join('data', 'model', f'{args.classname}.yml'+suffix), 'w') as f:
            f.write(data)
    elif args.command == 'gencode':
        models_dir = os.path.join('data', 'model')
        bm=BuildMaestro()
        for ymlfile in os_utils.get_file_list(models_dir, prefix=models_dir):
            if not ymlfile.endswith('.yml'):
                continue
            cfg = {}
            with open(ymlfile, 'r') as f:
                cfg=yaml.load(f)
            pyfile = os.path.join(make_package(cfg['package'].split('.')), cfg['name'].lower()+'.py')
            bm.add(GenerateModel(pyfile, ymlfile))
        bm.as_app(argp=argp)

if __name__ == '__main__': main()

import os, sys
from typing import List
def make_package(packageID: List[str], basedir: str = '.') -> str:
    pkgdir: str = ''
    for i in range(len(packageID)+1):
        pkgdir = os.path.join(basedir, *packageID[:i])
        if not os.path.isdir(pkgdir):
            print('Creating %s...' % (pkgdir,))
            os.makedirs(pkgdir)
        pkginit = os.path.join(pkgdir, '__init__.py')
        if not os.path.isfile(pkginit):
            print('Writing %s...' % (pkginit,) )
            with open(pkginit, 'w') as f:
                f.write('# Generated\n')
    return pkgdir
def main():
    import argparse
    argp = argparse.ArgumentParser()
    argp.add_argument('packages', metavar='package', nargs='+', help='Package name (e.g. vrchat.cache)')
    args = argp.parse_args()

    for package in args.packages:
        make_package(package.split('.'))
if __name__ == '__main__': main()

#!/bin/bash
PYTHON="$PYTHON"

# Prefer virtualenv python
[ -z "$PYTHON" ] && [ ! -z "${VIRTUAL_ENV}" ] && { PYTHON="${VIRTUAL_ENV}/bin/python"; }
[ -z "$PYTHON" ] && command -v python3.11 > /dev/null && { PYTHON=`command -v python3.11`; }
[ -z "$PYTHON" ] && command -v python3.10 > /dev/null && { PYTHON=`command -v python3.10`; }
[ -z "$PYTHON" ] && command -v python3 > /dev/null && { PYTHON=`command -v python3`; echo "WARNING: Using fallback interpreter!"; }

if [ ! -z "$PYTHON" ]; then
  #echo "Selected $PYTHON as interpreter."
  $PYTHON -m barotweak "$@"
else
  echo "Unable to find an available python3 interpreter. Please install python >=3.10."
  exit 1
fi

# Barotweak

A Python-based save editor for Barotrauma.

Capable of accessing and manipulating all kinds of internal values, this tool takes much of the BS out of save editing.
In addition, Barotweak will provide a suite of simple tools for fixing and checking the consistency of saves, which
will make the lives of server operators much easier.

**NOTE:** This is *very* early in development. Use at your own risk.

[[_TOC_]]

## Features

* **decompress** - Decompress saves
* **compress** - Compress saves
* **fix** - Automated save repairs/checks
  * **Campaign ID:** Fixes Campaign ID mismatch errors.
  * **Check Available Subs:** Checks if sub data is consistent.
* **sub rename** - Hurp
* **sub extract** - Extract submarines from `.save` files.

## Examples

### Decompressing a Save

```shell
# Decompress a save so it can be fiddled with manually or examined.
$ ./barotweak.sh decompress Saves/VRG1_0.save SaveData/
08/16/2020 05:25:45 AM [INFO    ]: Azimuth.sub -> SaveData/Azimuth.sub:
08/16/2020 05:25:45 AM [INFO    ]:   43,040B
08/16/2020 05:25:45 AM [INFO    ]: Dugong.sub -> SaveData/Dugong.sub:
08/16/2020 05:25:45 AM [INFO    ]:   37,014B
08/16/2020 05:25:45 AM [INFO    ]: gamesession.xml -> SaveData/gamesession.xml:
08/16/2020 05:25:45 AM [INFO    ]:   253,952B
08/16/2020 05:25:45 AM [INFO    ]: Typhon222223221222.sub -> SaveData/Typhon222223221222.sub:
08/16/2020 05:25:45 AM [INFO    ]:   356,309B
```


### Compressing a Save

```shell
# Recompressing that data.
# NOTE: Be sure to execute barotweak fix after recompressing.  It will fix a caching problem for you.
$ ./barotweak.cmd compress SaveData/ Saves/VRG1_0.save
08/16/2020 05:27:13 AM [INFO    ]: Saves/VRG1_0.save:
08/16/2020 05:27:13 AM [INFO    ]:   Azimuth.sub:
08/16/2020 05:27:13 AM [INFO    ]:     Internal Names: 26B
08/16/2020 05:27:13 AM [INFO    ]:     Data:           43,044B
08/16/2020 05:27:13 AM [INFO    ]:     Total:          43,070B
08/16/2020 05:27:13 AM [INFO    ]:   Dugong.sub:
08/16/2020 05:27:13 AM [INFO    ]:     Internal Names: 24B
08/16/2020 05:27:13 AM [INFO    ]:     Data:           37,018B
08/16/2020 05:27:13 AM [INFO    ]:     Total:          37,042B
08/16/2020 05:27:14 AM [INFO    ]:   gamesession.xml:
08/16/2020 05:27:14 AM [INFO    ]:     Internal Names: 34B
08/16/2020 05:27:14 AM [INFO    ]:     Data:           253,956B
08/16/2020 05:27:14 AM [INFO    ]:     Total:          253,990B
08/16/2020 05:27:15 AM [INFO    ]:   Typhon222223221222.sub:
08/16/2020 05:27:15 AM [INFO    ]:     Internal Names: 48B
08/16/2020 05:27:15 AM [INFO    ]:     Data:           356,313B
08/16/2020 05:27:15 AM [INFO    ]:     Total:          356,361B
08/16/2020 05:27:15 AM [INFO    ]:   Data:       690,463B
08/16/2020 05:27:15 AM [INFO    ]:   Compressed: 462,701B
```

### Renaming a Sub
```shell
# Rename anything sub-related called Typhon222223221222 to Typhon2.
# NOTE: Changing the savetime causes all clients to completely redownload the save, bypassing caching issues.
$ ./barotweak.cmd sub rename Saves/VRG1_0.save Typhon222223221222 Typhon2
08/16/2020 09:15:27 PM [INFO    ]: Directory 'tmp' exists, clearing out any files first...
08/16/2020 09:15:27 PM [INFO    ]: Decompressing...
08/16/2020 09:15:28 PM [INFO    ]: Loading session...
08/16/2020 09:15:28 PM [INFO    ]: Changed gamesess[submarine]
08/16/2020 09:15:28 PM [INFO    ]: Renaming tmp/Typhon222223221222.sub -> tmp/Typhon2.sub
08/16/2020 09:15:28 PM [INFO    ]: Renamed owned sub...
08/16/2020 09:15:28 PM [INFO    ]: Changing savetime...
08/16/2020 09:15:28 PM [INFO    ]: Writing session...
08/16/2020 09:15:28 PM [INFO    ]: Saves/VRG1_0.save:
08/16/2020 09:15:30 PM [INFO    ]:   Data:       686,971B
08/16/2020 09:15:30 PM [INFO    ]:   Compressed: 462,632B
```

### Checking a Save

```shell
# Check the save after screwing with it.
$ ./barotweak.cmd fix Saves/VRG1_0.save
08/16/2020 09:15:34 PM [INFO    ]: Directory 'tmp' exists, clearing out any files first...
08/16/2020 09:15:34 PM [INFO    ]: Decompressing...
08/16/2020 09:15:34 PM [INFO    ]: Loading session...
08/16/2020 09:15:34 PM [INFO    ]: Loading character data...
08/16/2020 09:15:35 PM [INFO    ]: Fix Campaign ID:
08/16/2020 09:15:35 PM [INFO    ]:   No problems
08/16/2020 09:15:35 PM [INFO    ]: Check Owned Subs:
08/16/2020 09:15:35 PM [INFO    ]:   OK
08/16/2020 09:15:35 PM [INFO    ]: Writing session...
08/16/2020 09:15:35 PM [INFO    ]: Writing character data...
08/16/2020 09:15:35 PM [INFO    ]: Saves/VRG1_0.save:
08/16/2020 09:15:36 PM [INFO    ]:   Data:       686,971B
08/16/2020 09:15:36 PM [INFO    ]:   Compressed: 462,632B
```

## Installing

### Prerequisites
* [Python](https://python.org) >= 3.8 with pip
* [git](https://git-scm.com)
* [git lfs](https://git-lfs.github.com/) for properly handling large files.

### Doin' It
```shell
$ git clone https://gitlab.com/N3X15/Barotweak.git
$ cd Barotweak
$ pip install -r requirements.txt
```

## How To

### Extract and Edit a Campaign Sub

1. Shut down the game or server.
1. Locate the *.save file you want to edit.
1. **Make a backup of it.**
1. Run `barotweak sub export path/to/my.save SubName path/to/Submarines/SubName.sub`.
1. Install any mods you want to use.
1. Open Barotrauma.
1. Make sure the mods are actually enabled in Settings.
1. Open the submarine editor.
1. Press F3 to open the console.
1. Enter `enablecheats` and ignore the warning.
1. Enter `fixhulls` to fix all the hull breaches that will cause all kinds of hull/gap weirdness if you leave them. Thank me later.
1. Start editing.
  * **THINGS TO BEAR IN MIND:**
    * Due to the submarine shifting around in-game, **everything will be off-grid** by a small amount. You will have to nudge things around (shift-WASD) after placing them. This won't harm anything.
    * Items players have dropped or screwed with will be shown.
    * Objects with damage will still have them.
  * Bugs to be aware of (that won't be fixed):
    * Engine power consumption returns to the save's version after being edited.  If you want to screw with engines, you will likely need to replace them entirely.
    * The sub's name only appears to affect in-game display. Either way, to avoid causing problems, don't screw with it.
1. Save and exit.
1. Overwrite the original `.sub` in the extracted data directory.
1. Run `barotweak sub import path/to/my.save SubName path/to/Submarines/SubName.sub` to recompress the save with the edited data.
1. If you're uploading the save, double-check the save permissions after uploading or you'll get weird errors about hash mismatches.
1. Finally, start up your game/server and enjoy.

## FAQ

### Why CLI?

Because headless servers are a thing.  Also, because of the way I've designed this, it would be fairly trivial to throw a GUI layer on top of it.

## License

MIT License

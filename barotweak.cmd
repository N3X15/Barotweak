@echo off
SET PYTHON=%PYTHON%
if [%PYTHON%]==[] (
  for %%x in (.venv\Scripts\python.exe, python3.11, python311, python3.10, python310, python, python3) do (
    WHERE %%x
    if %ERRORLEVEL%==0 (
      set PYTHON=%%x
      goto eggsit
    )
  )
)

:eggsit
if [%PYTHON%]==[] (
  echo ERROR: The command %PYTHON% is missing from PATH. Please install Python >= 3.10 from http://python.org and add it to PATH.
  exit 1
)
echo PYTHON=%PYTHON%

%PYTHON% -m barotweak %*

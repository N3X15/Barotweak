import sys
from typing import Optional
import gzip
import struct
import os
import hashlib
import logging
import re
from typing import BinaryIO
from pycsbinarywriter import cstypes

from buildtools import os_utils
from buildtools.bt_logging import IndentLogger
log = IndentLogger(logging.getLogger(__name__))

from lxml import etree

LXML_WRITE_OPTS = dict(pretty_print=True, xml_declaration=True, encoding='utf-8')
REG_STRIP_WHITESPACE = re.compile(r'\s+')

SESSIONFILE = os.path.join('tmp', 'gamesession.xml')

def decompressFile(f: BinaryIO, outdir: str, verbose: bool = False) -> bool:
    nameLength = cstypes.int32.unpackFrom(f)
    # Out of bytes
    if nameLength is None:
        return False
    if nameLength > 255:
        raise Exception(f'Corrupt File(?): Filename is {nameLength:,}B long')
    name = ''
    for _ in range(nameLength):
        name += cstypes.char.unpackFrom(f)

    fileLength = cstypes.int32.unpackFrom(f)
    path = os.path.join(outdir, name)
    subdir = os.path.dirname(path)
    if not os.path.isdir(subdir):
        os.makedirs(subdir)
    with open(path, 'wb') as df:
        for _ in range(fileLength):
            df.write(f.read(1))
    if verbose:
        with log.info(f'{name} -> {path}:'):
            log.info(f'{fileLength:,}B')

    basename, _ = os.path.splitext(path)
    if path.endswith('.sub'):
        extractSubToFile(path, basename+'.sub.xml')

    return True

def decompressFilesIn(filename: str, todir: str, verbose: bool = False) -> None:
    with gzip.open(filename, 'rb') as f:
        while decompressFile(f, todir, verbose):
            pass

def compressFileTo(filename: str, internalName: str, f: BinaryIO, verbose: bool = False) -> int:
    lenInternalName: int = len(internalName)
    lenData: int = 0

    f.write(cstypes.int32.pack(lenInternalName))
    for i in range(lenInternalName):
        f.write(cstypes.char.pack(internalName[i]))
    lenInternalName = cstypes.int32.size + (lenInternalName * cstypes.char.size)

    lenFile: int = os.path.getsize(filename)
    f.write(cstypes.int32.pack(lenFile))
    with open(filename, 'rb') as rf:
        for _ in range(lenFile):
            f.write(rf.read(1))
    lenFile += cstypes.int32.size

    if verbose:
        with log.info(f'{internalName}:'):
            log.info(f'Internal Names: {lenInternalName:,}B')
            log.info(f'Data:           {lenFile:,}B')
            log.info(f'Total:          {(lenFile+lenInternalName):,}B')
    return lenInternalName+lenFile

def loadGameSession(filename: Optional[str] = None, minify: bool = False) -> etree.ElementTree:
    parser = etree.XMLParser(remove_blank_text=minify) # Minify here
    log.info('Loading session...')
    return etree.parse(filename or SESSIONFILE, parser=parser)

def saveGameSession(session: etree.ElementTree, filename: Optional[str] = None, minify: bool = False) -> None:
    log.info('Writing session...')
    session.write(filename or SESSIONFILE, pretty_print=not minify, xml_declaration=True, encoding='utf-8')

def loadCharacterData(filename: str, minify: bool = False) -> etree.ElementTree:
    parser = etree.XMLParser(remove_blank_text=minify) # Minify here
    log.info('Loading character data...')
    return etree.parse(filename, parser=parser)

def saveCharacterData(chardata: etree.ElementTree, filename: str, minify: bool = False) -> None:
    log.info('Writing character data...')
    chardata.write(filename, pretty_print=not minify, xml_declaration=True, encoding='utf-8')

def calcHashOfXML(xdoc: etree.Element) -> etree.Element:
    doc = etree.tostring(xdoc, encoding='ascii')
    doc = REG_STRIP_WHITESPACE.sub('', doc)
    h = hashlib.md5(doc).hexdigest()
    return etree.Element('Md5Hash', {'Hash': h, 'ShortHash': h[0:7]})

def readSubToStr(filename: str) -> str:
    # Wow I can use streams!!!!
    with gzip.open(filename, 'rb') as f:
        return f.read().decode('utf-8')

def extractSubToFile(subfile: str, xmlfile: str) -> str:
    # Wow I can use streams!!!!
    with gzip.open(subfile, 'rb') as f:
        with open(xmlfile, 'wb') as w:
            w.write(f.read())

def writeSubFromStr(filename: str, data: str) -> None:
    # Wow I can use streams!!!!
    with gzip.open(filename, 'wb') as f:
        return f.read().decode('utf-8')

def readSubToXML(filename: str) -> etree.ElementTree:
    parser = etree.XMLParser(remove_blank_text=False)
    with gzip.open(filename, 'rb') as f:
        return etree.parse(f, parser=parser)

def writeSubFromXML(filename: str, xml: etree.ElementTree) -> None:
    with gzip.open(filename, 'wb') as f:
        f.write(xml.tostring(pretty_print=True, xml_declaration=True, encoding='utf-8'))

def compressFilesIn(fromdir: str, tofile: str, verbose: bool = False) -> None:
    actual = 0
    with log.info(tofile+':'):
        with gzip.open(tofile, 'wb') as f:
            for filename in os_utils.get_file_list(fromdir):
                if not filename.endswith('.sub.xml'):
                    actual += compressFileTo(os.path.join(fromdir, filename), filename, f)
        if verbose:
            log.info(f'Data:       {actual:,}B')
            log.info(f'Compressed: {os.path.getsize(tofile):,}B')

import os, sys, logging

from buildtools import os_utils, log

from barotweak.io import decompressFilesIn

def _register_decompress_subp(subp):
    decomp = subp.add_parser('decompress', aliases=['d'], help='Decompress binary *.save files.')
    decomp.add_argument('infile', type=str, help='Input savefile')
    decomp.add_argument('outdir', type=str, help='output directory')
    decomp.set_defaults(command=handle_decompress)

def handle_decompress(args):
    if not os.path.isfile(args.infile):
        log.critical('Save file %r doesn\'t exist.', args.infile)
        sys.exit(1)
    if os.path.isdir(args.outdir):
        log.info('Directory %r exists, clearing out any files first...', args.outdir)
        os_utils.safe_rmtree(args.outdir)
    decompressFilesIn(args.infile, args.outdir, True)

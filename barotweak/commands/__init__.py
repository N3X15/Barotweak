from .decompress import _register_decompress_subp
from .compress import _register_compress_subp
from .fix import _register_fix_subp
from .sub import _register_sub_subp
from .show import _register_show_subp

def register_commands(subp):
    _register_compress_subp(subp)
    _register_decompress_subp(subp)
    _register_fix_subp(subp)
    _register_show_subp(subp)
    _register_sub_subp(subp)

import os, sys, logging

from lxml import etree

from buildtools import os_utils, log

from barotweak.io import decompressFilesIn, compressFilesIn, loadGameSession, saveGameSession, loadCharacterData, saveCharacterData
from barotweak.utils import fixSaveTime
from barotweak.fixes import ALL_FIXES

SESSIONFILE = os.path.join('tmp', 'gamesession.xml')

def _register_fix_subp(subp):
    fixp = subp.add_parser('fix', help='Fix common save problems.')
    fixp.add_argument('subject', type=str, help='Input savefile (paired with _CharacterData.xml)')
    #fixp.add_argument('--minify', action='store_true', default=False, help='De-prettify internal XML.  Shouldn\'t break anything...')
    for fix in ALL_FIXES.values():
        fix.register_args(fixp)
    fixp.set_defaults(command=handle_fix)


def handle_fix(args):
    if not os.path.isfile(args.subject):
        log.critical('Save file %r doesn\'t exist.', args.infile)
        sys.exit(1)

    if os.path.isdir('tmp'):
        log.info('Directory %r exists, clearing out any files first...', 'tmp')
        os_utils.safe_rmtree('tmp')

    os_utils.ensureDirExists('tmp')

    # filename, ext
    basename, _ = os.path.splitext(args.subject)
    characterdatafile = f'{basename}_CharacterData.xml'

    decompressFilesIn(args.subject, 'tmp', False)

    session = loadGameSession()
    chardata = loadCharacterData(characterdatafile)

    for name, fix in ALL_FIXES.items():
        with log.info(f'{name}:'):
            fix.fix(args, session)

    fixSaveTime(session)

    saveGameSession(session)
    saveCharacterData(chardata, characterdatafile)

    compressFilesIn('tmp', args.subject, True)

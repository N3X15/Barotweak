import os, sys, logging

from lxml import etree

from buildtools import os_utils, log

from barotweak.io import decompressFilesIn, compressFilesIn, loadGameSession, saveGameSession, loadCharacterData, saveCharacterData
from barotweak.utils import fixSaveTime
from barotweak.fixes import ALL_FIXES
from barotweak.model.gamesession import GameSession

SESSIONFILE = os.path.join('tmp', 'gamesession.xml')

def _register_show_subp(subp):
    showp = subp.add_parser('show', help='Show information about a save.')
    showp.add_argument('subject', type=str, help='Input savefile (paired with _CharacterData.xml)')
    showp.set_defaults(command=handle_show)


def handle_show(args):
    if not os.path.isfile(args.subject):
        log.critical('Save file %r doesn\'t exist.', args.infile)
        sys.exit(1)

    if os.path.isdir('tmp'):
        log.info('Directory %r exists, clearing out any files first...', 'tmp')
        os_utils.safe_rmtree('tmp')

    os_utils.ensureDirExists('tmp')

    # filename, ext
    basename, _ = os.path.splitext(args.subject)
    characterdatafile = f'{basename}_CharacterData.xml'

    decompressFilesIn(args.subject, 'tmp', False)

    session = GameSession.LoadGameSession(os.path.join('tmp', 'gamesession.xml'))
    #chardata = loadCharacterData(characterdatafile)

    session.display()

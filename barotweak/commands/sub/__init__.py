from .rename import _register_sub_rename
from .import_ import _register_sub_import
from .export import _register_sub_export

def _register_sub_subp(subp):
    subsp = subp.add_parser('sub', aliases=['subs'], help='Submarine-related shit')

    subs_subp = subsp.add_subparsers()

    _register_sub_rename(subs_subp)
    _register_sub_export(subs_subp)
    _register_sub_import(subs_subp)

import os, sys, logging, shutil

from buildtools import os_utils, log

from barotweak.io import decompressFilesIn, compressFilesIn, loadGameSession, saveGameSession
#from barotweak.utils import fixSaveTime
from barotweak.fixes import ALL_FIXES


def _register_sub_export(subp):
    renamep = subp.add_parser('export', help='Export a .sub from a campaign .save.')
    renamep.add_argument('subject', type=str, help='Input savefile.')
    renamep.add_argument('subname', type=str, help='Name of the sub in the .save file.')
    renamep.add_argument('tofile', type=str, help='Filename to which you wish to export the sub. It would be a good idea to use the .sub extension.')
    #renamep.add_argument('--minify', action='store_true', default=False, help='De-prettify internal XML.  Shouldn\'t break anything...')
    renamep.set_defaults(command=handle_sub_export)

def handle_sub_export(args):
    if not os.path.isfile(args.subject):
        log.critical('Save file %r doesn\'t exist.', args.subject)
        sys.exit(1)

    if os.path.isdir('tmp'):
        log.info('Directory %r exists, clearing out any files first...', 'tmp')
        os_utils.safe_rmtree('tmp')

    os_utils.ensureDirExists('tmp')

    # filename, ext
    basename, _ = os.path.splitext(args.subject)
    characterdatafile = f'{basename}_CharacterData.xml'

    log.info('Decompressing...')
    decompressFilesIn(args.subject, 'tmp', False)

    srcsubfile = os.path.join('tmp', f'{args.subname}.sub')

    if not os.path.isfile(srcsubfile):
        log.critical('Submarine %r does not exist in the .save file (expected to see a file called %r after decompression).', args.subname, srcsubfile)
        sys.exit(1)

    if os.path.isfile(args.tofile):
        log.critical('Exported .sub file %r would overwrite something that already exists. Aborting.', args.tofile)
        sys.exit(1)

    os_utils.single_copy(srcsubfile, args.tofile, verbose=True, ignore_mtime=True)

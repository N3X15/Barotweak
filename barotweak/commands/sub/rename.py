import os, sys, logging, shutil

from lxml import etree

from buildtools import os_utils, log

from barotweak.io import decompressFilesIn, compressFilesIn, loadGameSession, saveGameSession
from barotweak.utils import fixSaveTime
from barotweak.fixes import ALL_FIXES


def _register_sub_rename(subp):
    renamep = subp.add_parser('rename', help='rename a sub')
    renamep.add_argument('subject', type=str, help='Input savefile (paired with _CharacterData.xml)')
    renamep.add_argument('fromname', type=str, help='Original name of the sub.')
    renamep.add_argument('toname', type=str, help='New name of the sub.')
    #renamep.add_argument('--minify', action='store_true', default=False, help='De-prettify internal XML.  Shouldn\'t break anything...')
    renamep.set_defaults(command=handle_sub_rename)

def handle_sub_rename(args):
    if not os.path.isfile(args.subject):
        log.critical('Save file %r doesn\'t exist.', args.subject)
        sys.exit(1)

    if os.path.isdir('tmp'):
        log.info('Directory %r exists, clearing out any files first...', 'tmp')
        os_utils.safe_rmtree('tmp')

    os_utils.ensureDirExists('tmp')

    # filename, ext
    basename, _ = os.path.splitext(args.subject)
    characterdatafile = f'{basename}_CharacterData.xml'

    log.info('Decompressing...')
    decompressFilesIn(args.subject, 'tmp', False)

    oldsubfile = os.path.join('tmp', f'{args.fromname}.sub')
    newsubfile = os.path.join('tmp', f'{args.toname}.sub')

    if not os.path.isfile(oldsubfile):
        log.critical('Old sub file %r does not exist.', oldsubfile)
        sys.exit(1)
    if os.path.isfile(newsubfile):
        log.critical('New sub file %r would overwrite something that already exists. Aborting.', newsubfile)
        sys.exit(1)

    session = loadGameSession()

    gamesess = session.xpath('//Gamesession')[0]
    if gamesess.attrib['submarine'] == args.fromname:
        log.info('Changed gamesess[submarine]')
        gamesess.attrib['submarine'] = args.toname

    log.info('Renaming %s -> %s', oldsubfile, newsubfile)
    shutil.move(oldsubfile, newsubfile)

    for ownedsub in gamesess.find('ownedsubmarines'):
        if ownedsub.get('name') == args.fromname:
            ownedsub.attrib['name'] = args.toname
            log.info('Renamed owned sub...')
            break

    fixSaveTime(session)

    saveGameSession(session)

    compressFilesIn('tmp', args.subject, True)

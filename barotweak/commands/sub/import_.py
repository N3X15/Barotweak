import os, sys, logging, shutil

from buildtools import os_utils, log

from barotweak.io import decompressFilesIn, compressFilesIn, loadGameSession, saveGameSession
#from barotweak.utils import fixSaveTime
from barotweak.fixes import ALL_FIXES


def _register_sub_import(subp):
    renamep = subp.add_parser('import', help='Inject a .sub into a campaign .save.')
    renamep.add_argument('subject', type=str, help='Input savefile.')
    renamep.add_argument('subname', type=str, help='Name of the sub in the .save file.')
    renamep.add_argument('fromfile', type=str, help='Filename from which you wish to import the sub.')
    #renamep.add_argument('--minify', action='store_true', default=False, help='De-prettify internal XML.  Shouldn\'t break anything...')
    renamep.add_argument('--add-as-owned', dest='as_owned', action='store_true', default=False, help='If not present, add it as an owned submarine.')
    renamep.set_defaults(command=handle_sub_import)

def handle_sub_import(args):
    if not os.path.isfile(args.subject):
        log.critical('Save file %r doesn\'t exist.', args.subject)
        sys.exit(1)

    if os.path.isdir('tmp'):
        log.info('Directory %r exists, clearing out any files first...', 'tmp')
        os_utils.safe_rmtree('tmp')

    os_utils.ensureDirExists('tmp')

    # filename, ext
    basename, _ = os.path.splitext(args.subject)
    characterdatafile = f'{basename}_CharacterData.xml'

    log.info('Decompressing...')
    decompressFilesIn(args.subject, 'tmp', False)

    destsubfile = os.path.join('tmp', f'{args.subname}.sub')

    if not os.path.isfile(destsubfile):
        if not args.as_owned:
            log.critical('Submarine %r does not exist in the .save file (expected to see a file called %r after decompression).', args.subname, srcsubfile)
            sys.exit(1)
        log.warning('Could not find sub %r, adding it as an owned sub.', args.subname)
        sess = loadGameSession()
        ownedsubs = sess.xpath('//ownedsubmarines')[0]
        etree.SubElement(ownedsubs, 'sub', {'name', args.subname})
        saveGameSession(sess)

    os_utils.single_copy(args.fromfile, destsubfile, verbose=True, ignore_mtime=True)

    compressFilesIn('tmp', args.subject, True)

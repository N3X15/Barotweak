import os, sys, logging
from barotweak.io import compressFilesIn, loadGameSession, saveGameSession#, loadSubmarine, saveSubmarine
from buildtools import log
def _register_compress_subp(subp):
    comp = subp.add_parser('compress', aliases=['c'], help='Recompress extracted saves.')
    comp.set_defaults(command=handle_compress)
    comp.add_argument('indir', type=str, help='Input directory')
    comp.add_argument('outfile', type=str, help='Output gzipped file')
    comp.add_argument('--minify', action='store_true', default=False, help='De-prettify internal XML.  Shouldn\'t break anything...')

def handle_compress(args):
    if not os.path.isdir(args.indir):
        log.critical('Directory doesn\'t exist.')
        sys.exit(1)
    if args.minify:
        sessfile = os.path.join(args.indir, 'gamesession.xml')
        with log.info('Recompressing gamesession.xml...'):
            saveGameSession(loadGameSession(sessfile, minify=minify), sessfile, minify=minify)
    compressFilesIn(args.indir, args.outfile, True)

from lxml import etree
from buildtools import log

def fixSaveTime(sessiontree: etree.ElementTree, verbose: bool = True) -> None:
    session = sessiontree.xpath('//Gamesession')[0]
    oldtime: int = int(session.get('savetime'))
    newtime: int = oldtime+1
    if verbose:
        log.info(f'Changing savetime to fix caching issues ({oldtime} -> {newtime})...')
    session.set('savetime', str(newtime))

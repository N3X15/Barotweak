# Saves contain 3 major things (that I know of)
# 1. Game session (file.save/gamesession.xml)
# 2. Subs (file.save/*.sub)
# 3. Character data (file_CharacterData.xml)
# This object combines all 3 and attempts to interpret it logically.

import gzip
import os
import sys
from barotweak.io import LXML_WRITE_OPTS
from typing import BinaryIO
from pycsbinarywriter import cstypes
from lxml import etree

class Save(object):
    def __init__(self) -> None:
        self.session: GameSession = None
        self.subs: Dict[str, Submarine] = {}
        self.characterdata: CharacterData = None

    @classmethod
    def Load(filename: str) -> 'Save':
        save = cls()
        size = os.path.getfilesize(filename)
        read = 0
        with gzip.open(filename, 'rb') as f:
            nameLength: int = cstypes.int32.unpackFrom(f)
            read += 4
            assert nameLength > 4, 'Compressed filename too short.'
            assert nameLength <= 255, 'Compressed filename too long.'
            name: str = ''
            for _ in range(nameLength):
                name += cstypes.char.unpackFrom(f)
            read += nameLength * 2

            fileLength: int = cstypes.int32.unpackFrom(f)
            read += 4
            assert fileLength > 0
            assert fileLength <= (size-read)
            save._handleStreamed(name, f.read(fileLength))
            read += fileLength

    def _handleStreamed(self, bucketid: str, bucketdata: bytes) -> None:
        if bucketid == 'gamesession.xml':
            print('Loading session...', end='')
            self._handleGameSettings(bucketdata.decode('utf-8'))
            print(' Done.')
        elif bucketid.endswith('.sub'):
            print(f'Loading sub {bucketid[:-4]!r}...', end='')
            self._handleSubmarine(bucketid, bucketdata)
            print(' Done.')

    def _handleGameSettings(self, gsstr: str) -> None:
        tree = etree.fromstring(filename)
        self.session = cls()
        self.session.deserialize(tree.getroot())

    def _handleSubmarine(self, subfilename: str, subdata: bytes) -> None:
        tree = etree.fromstring(gzip.decompress(subdata))
        sub = Submarine()
        sub.deserialize(tree.getroot())
        self.subs[subfilename[:-4]] = sub

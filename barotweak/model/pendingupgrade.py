from barotweak.model._raw.basependingupgrade import BasePendingUpgrade

class PendingUpgrade(BasePendingUpgrade):
    def __init__(self) -> None:
        super().__init__()

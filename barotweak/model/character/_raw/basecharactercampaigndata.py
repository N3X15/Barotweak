from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
from typing import Optional
class BaseCharacterCampaignData(BaseSerializable):
    TAG = 'CharacterCampaignData'
    def __init__(self):
        super().__init__()
        self.name: str = ''
        self.endpoint: int = -1
        self.steamid: int = -1
        self.character: Optional[TYPE] = None
        self.inventory: Optional[TYPE] = None
        self.health: Optional[TYPE] = None
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['name'] = str(self.name)
        o['endpoint'] = str(self.endpoint)
        o['steamid'] = str(self.steamid)
        if self.character is not None:
            o.append(self.character.serialize())
        if self.inventory is not None:
            o.append(self.inventory.serialize())
        if self.health is not None:
            o.append(self.health.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.name = self._getStr('name', required=True)
        self.endpoint = self._getInt('endpoint', required=True)
        self.steamid = self._getInt('steamid', required=True)
        e: Optional[etree.Element] = data.find('Character')
        self.character = None
        if e is not None:
            self.character = FACTORY_METHOD()
            self.character.deserialize(e)
        e: Optional[etree.Element] = data.find('inventory')
        self.inventory = None
        if e is not None:
            self.inventory = FACTORY_METHOD()
            self.inventory.deserialize(e)
        e: Optional[etree.Element] = data.find('health')
        self.health = None
        if e is not None:
            self.health = FACTORY_METHOD()
            self.health.deserialize(e)

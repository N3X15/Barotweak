from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
class BaseSkill(BaseSerializable):
    TAG = 'skill'
    def __init__(self):
        super().__init__()
        self.identifier: str = ''
        self.level: float = -1.0
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['identifier'] = str(self.identifier)
        o['level'] = str(self.level)
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.identifier = self._getStr('identifier', required=True)
        self.level = self._getFloat('level', required=True)

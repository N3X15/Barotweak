from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.character.skill import Skill
from lxml import etree
from typing import List, Optional
class BaseJob(BaseSerializable):
    TAG = 'job'
    def __init__(self):
        super().__init__()
        self.name: str = ''
        self.identifier: str = ''
        self.skill: List[Skill] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['name'] = str(self.name)
        o['identifier'] = str(self.identifier)
        for e in self.skill:
            o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.name = self._getStr('name', required=True)
        self.identifier = self._getStr('identifier', required=True)
        self.skill = []
        for el in data.findall('skill'):
            o = Skill()
            o.deserialize(el)
            self.skill += [o]

from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.character.charactercampaigndata import CharacterCampaignData
from lxml import etree
from typing import List, Optional
class BaseCharacterData(BaseSerializable):
    TAG = 'CharacterData'
    def __init__(self):
        super().__init__()
        self.charactercampaigndata: List[CharacterCampaignData] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        for e in self.charactercampaigndata:
            o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.charactercampaigndata = []
        for el in data.findall('CharacterCampaignData'):
            o = CharacterCampaignData()
            o.deserialize(el)
            self.charactercampaigndata += [o]

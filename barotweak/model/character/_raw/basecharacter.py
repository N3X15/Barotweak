from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.character.job import Job
from lxml import etree
import sys
from typing import Optional
class BaseCharacter(BaseSerializable):
    TAG = 'Character'
    def __init__(self):
        super().__init__()
        self.name: str = ''
        self.speciesname: str = ''
        self.gender: str = ''
        self.race: str = ''
        self.salary: int = -1
        self.headspriteid: int = -1
        self.hairindex: int = -1
        self.beardindex: int = -1
        self.moustacheindex: int = -1
        self.faceattachmentindex: int = -1
        self.startitemsgiven: bool = False
        self.ragdoll: str = ''
        self.personality: str = ''
        self.hull: Optional[int] = None
        self.job: Job = Job()
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['name'] = str(self.name)
        o['speciesname'] = str(self.speciesname)
        o['gender'] = str(self.gender)
        o['race'] = str(self.race)
        o['salary'] = str(self.salary)
        o['headspriteid'] = str(self.headspriteid)
        o['hairindex'] = str(self.hairindex)
        o['beardindex'] = str(self.beardindex)
        o['moustacheindex'] = str(self.moustacheindex)
        o['faceattachmentindex'] = str(self.faceattachmentindex)
        o['startitemsgiven'] = str(self.startitemsgiven)
        o['ragdoll'] = str(self.ragdoll)
        o['personality'] = str(self.personality)
        if self.hull != None:
            o['hull'] = str(self.hull)
        o.append(self.job.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.name = self._getStr('name', required=True)
        self.speciesname = self._getStr('speciesname', required=True)
        self.gender = self._getStr('gender', required=True)
        self.race = self._getStr('race', required=True)
        self.salary = self._getInt('salary', required=True)
        self.headspriteid = self._getInt('headspriteid', required=True)
        self.hairindex = self._getInt('hairindex', required=True)
        self.beardindex = self._getInt('beardindex', required=True)
        self.moustacheindex = self._getInt('moustacheindex', required=True)
        self.faceattachmentindex = self._getInt('faceattachmentindex', required=True)
        self.startitemsgiven = self._getBool('startitemsgiven', required=True)
        self.ragdoll = self._getStr('ragdoll', required=True)
        self.personality = self._getStr('personality', required=True)
        self.hull = self._getInt('hull', default=None, required=False)
        e: Optional[etree.Element] = data.find('job')
        self.job = None
        if e is None:
            print('Element <job> cannot be found in element <Character>!')
            print('This likely means the save is corrupt. :(')
            sys.exit(1)
        self.job = Job()
        self.job.deserialize(e)

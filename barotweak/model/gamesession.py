from barotweak.model._raw.basegamesession import BaseGameSession
import random
from enum import IntEnum, auto
from lxml import etree

class ESaveType(IntEnum):
    MULTIPLAYER_CAMPAIGN = auto()
    SINGLEPLAYER_CAMPAIGN = auto()
    UNKNOWN = auto()

class GameSession(BaseGameSession):
    TAG = 'Gamesession'
    def __init__(self):
        super().__init__()
        self.singleplayercampaign = None

    @classmethod
    def LoadGameSession(cls, filename: str, minify: bool = False) -> 'GameSession':
        parser = etree.XMLParser(remove_blank_text=minify)
        tree = etree.parse(filename, parser=parser)
        sess = cls()
        sess.deserialize(tree.getroot())
        return sess

    def save(self, filename: str, minify: bool = False) -> None:
        root = self.serialize()
        with open(filename, 'wb') as f:
            f.write(etree.tostring(root, xml_declaration=True, pretty_print=not minify, encoding='utf-8'))

    def getSaveType(self) -> ESaveType:
        if self.multiplayercampaign is not None:
            return ESaveType.MULTIPLAYER_CAMPAIGN
        elif self.singleplayercampaign is not None:
            return ESaveType.SINGLEPLAYER_CAMPAIGN
        return ESaveType.UNKNOWN

    def display(self) -> None:
        print(f'Version: {self.version}')
        print(f'Time: {self.savetime}')
        print(f'Save type: {self.getSaveType().name}')
        print('Content Packages:')
        for pkg in self.selectedcontentpackages:
            print(f' * {pkg}')
        print('Owned Subs:')
        for sub in self.ownedsubmarines:
            print(f' * {sub}')

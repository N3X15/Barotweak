from enum import IntEnum, auto
from typing import Optional, Dict, Any, Type
from lxml import etree
from buildtools import log

class BaseSerializable(object):
    TAG = 'FIXME'

    def __init__(self, tag: Optional[str] = None) -> None:
        self.tag: str = tag or self.TAG
        self.raw: etree.Element = etree.Element(self.tag)

    def serialize(self) -> etree.Element:
        return etree.Element(self.tag)

    def deserialize(self, data: etree.Element) -> None:
        self.raw = data

    def _getRaw(self, key: str, default: Any, data: Optional[etree.Element] = None, required: bool = False) -> Any:
        if data is None:
            data = self.raw
        return data.get(key, default)
    def _setRaw(self, key: str, value: Any, data: Optional[etree.Element] = None, required: bool = False) -> Any:
        if data is None:
            data = self.raw
        data.set(key, value)

    def _getBool(self, key: str, default: bool=False, data: Optional[etree.Element] = None, required: bool = False) -> bool:
        return bool(self._getRaw(key, default, data) or default)
    def _setBool(self, key: str, value: bool, data: Optional[etree.Element] = None, required: bool = False) -> None:
        self._setRaw(key, 'true' if value else 'false', data)

    def _getInt(self, key: str, default: int=0, data: Optional[etree.Element] = None, required: bool = False) -> int:
        return int(self._getRaw(key, default, data) or default)
    def _setInt(self, key: str, value: int, data: Optional[etree.Element] = None, required: bool = False) -> None:
        self._setRaw(key, str(value), data)

    def _getStr(self, key: str, default: str="", data: Optional[etree.Element] = None, required: bool = False) -> str:
        return str(self._getRaw(key, default, data) or default)
    def _setStr(self, key: str, value: str, data: Optional[etree.Element] = None, required: bool = False) -> None:
        self._setRaw(key, str(value), data)

    def _getFloat(self, key: str, default: float=0.0, data: Optional[etree.Element] = None, required: bool = False) -> float:
        return float(self._getRaw(key, default, data) or default)
    def _setFloat(self, key: str, value: float, data: Optional[etree.Element] = None, required: bool = False) -> None:
        self._setRaw(key, str(value), data)

    def _getList(self, key: str, default: list=[], delim: str = ', ', data: Optional[etree.Element] = None, required: bool = False) -> list:
        o = self._getRaw(key, default, data)
        if o is None:
            return default
        return o.split(delim)
    def _setList(self, key: str, value: list, delim: str = ', ', data: Optional[etree.Element] = None, required: bool = False) -> None:
        self._setRaw(key, delim.join([str(v) for v in value]), data)

    def _getChild(self, tag: str, type: Type['BaseSerializable'], data: Optional[etree.Element] = None, required: bool = False) -> Optional['BaseSerializable']:
        if data is None:
            data = self.raw
        e = data.find(tag)
        if e is None:
            if required:
                log.error(f'ERROR: Tag %r does not exist in %r', tag, self.TAG)
                sys.exit(1)
            return None
        else:
            o = type()
            o.deserialize(e)
            return o

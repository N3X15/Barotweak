from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
class BasePendingUpgrade(BaseSerializable):
    TAG = 'PendingUpgrade'
    def __init__(self):
        super().__init__()
        self.category: str = ''
        self.prefab: str = ''
        self.level: int = -1
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['category'] = str(self.category)
        o['prefab'] = str(self.prefab)
        o['level'] = str(self.level)
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.category = self._getStr('category', required=True)
        self.prefab = self._getStr('prefab', required=True)
        self.level = self._getInt('level', required=True)

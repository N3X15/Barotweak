from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.pendingupgrade import PendingUpgrade
from lxml import etree
from typing import List, Optional
class BasePendingUpgrades(BaseSerializable):
    TAG = 'PendingUpgrades'
    def __init__(self):
        super().__init__()
        self.pendingupgrades: List[PendingUpgrade] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        for e in self.pendingupgrades:
            o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.pendingupgrades = []
        for el in data.findall('PendingUpgrade'):
            o = PendingUpgrade()
            o.deserialize(el)
            self.pendingupgrades += [o]

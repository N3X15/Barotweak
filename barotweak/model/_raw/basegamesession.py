from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.multiplayercampaign import MultiPlayerCampaign
from lxml import etree
from typing import List, Optional
class BaseGameSession(BaseSerializable):
    TAG = 'Gamesession'
    def __init__(self):
        super().__init__()
        self.savetime: int = -1
        self.version: str = ''
        self.submarine: str = ''
        self.mapseed: str = ''
        self.selectedcontentpackages: List[str] = []
        self.campaignid: int = -1
        self.ownedsubmarines: List[str] = []
        self.multiplayercampaign: Optional[MultiPlayerCampaign] = None
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['savetime'] = str(self.savetime)
        o['version'] = str(self.version)
        o['submarine'] = str(self.submarine)
        o['mapseed'] = str(self.mapseed)
        o['selectedcontentpackages'] = '|'.join([str(x) for x in self.selectedcontentpackages])
        o['campaignid'] = str(self.campaignid)
        e = etree.Element('ownedsubmarines')
        for value in self.ownedsubmarines:
            inner = etree.Element('sub')
            inner['name'] = str(value)
            e.append(inner)
        o.append(e)
        if self.multiplayercampaign is not None:
            o.append(self.multiplayercampaign.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.savetime = self._getInt('savetime', required=True)
        self.version = self._getStr('version', required=True)
        self.submarine = self._getStr('submarine', required=True)
        self.mapseed = self._getStr('mapseed', required=True)
        self.selectedcontentpackages = [str(x.strip()) for x in data.get('selectedcontentpackages', '').split('|')]
        self.campaignid = self._getInt('campaignid', required=True)
        self.ownedsubmarines = []
        for a in data.find('ownedsubmarines'):
            self.ownedsubmarines.append(str(a.attrib['name']))
        e: Optional[etree.Element] = data.find('MultiPlayerCampaign')
        self.multiplayercampaign = None
        if e is not None:
            self.multiplayercampaign = MultiPlayerCampaign()
            self.multiplayercampaign.deserialize(e)

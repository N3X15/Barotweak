from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.character.character import Character
from lxml import etree
from typing import List, Optional
class BaseBots(BaseSerializable):
    TAG = 'bots'
    def __init__(self):
        super().__init__()
        self.hasbots: bool = False
        self.characters: List[Character] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['hasbots'] = str(self.hasbots)
        for e in self.characters:
            o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.hasbots = self._getBool('hasbots', required=True)
        self.characters = []
        for el in data.findall('Character'):
            o = Character()
            o.deserialize(el)
            self.characters += [o]

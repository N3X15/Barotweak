from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
from typing import List, Optional
class BaseSubmarine(BaseSerializable):
    TAG = 'Submarine'
    def __init__(self):
        super().__init__()
        self.description: str = ''
        self.checkval: int = -1
        self.price: int = -1
        self.initialsuppliesspawned: bool = False
        self.type: str = ''
        self.class_: str = ''
        self.tags: int = -1
        self.gameversion: str = ''
        self.dimensions: str = ''
        self.recommendedcrewsizemin: int = -1
        self.recommendedcrewsizemax: int = -1
        self.recommendedcrewexperience: str = ''
        self.requiredcontentpackages: str = ''
        self.name: str = ''
        self.previewimage: str = ''
        self.items: List[Item] = []
        self.gaps: List[Gap] = []
        self.structures: List[Structure] = []
        self.waypoints: List[WayPoint] = []
        self.hulls: List[Hull] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['description'] = str(self.description)
        o['checkval'] = str(self.checkval)
        o['price'] = str(self.price)
        o['initialsuppliesspawned'] = str(self.initialsuppliesspawned)
        o['type'] = str(self.type)
        o['class'] = str(self.class_)
        o['tags'] = str(self.tags)
        o['gameversion'] = str(self.gameversion)
        o['dimensions'] = str(self.dimensions)
        o['recommendedcrewsizemin'] = str(self.recommendedcrewsizemin)
        o['recommendedcrewsizemax'] = str(self.recommendedcrewsizemax)
        o['recommendedcrewexperience'] = str(self.recommendedcrewexperience)
        o['requiredcontentpackages'] = str(self.requiredcontentpackages)
        o['name'] = str(self.name)
        o['previewimage'] = str(self.previewimage)
        for e in self.items:
            o.append(e.serialize())
        for e in self.gaps:
            o.append(e.serialize())
        for e in self.structures:
            o.append(e.serialize())
        for e in self.waypoints:
            o.append(e.serialize())
        for e in self.hulls:
            o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.description = self._getStr('description', required=True)
        self.checkval = self._getInt('checkval', required=True)
        self.price = self._getInt('price', required=True)
        self.initialsuppliesspawned = self._getBool('initialsuppliesspawned', required=True)
        self.type = self._getStr('type', required=True)
        self.class_ = self._getStr('class', required=True)
        self.tags = self._getInt('tags', required=True)
        self.gameversion = self._getStr('gameversion', required=True)
        self.dimensions = self._getStr('dimensions', required=True)
        self.recommendedcrewsizemin = self._getInt('recommendedcrewsizemin', required=True)
        self.recommendedcrewsizemax = self._getInt('recommendedcrewsizemax', required=True)
        self.recommendedcrewexperience = self._getStr('recommendedcrewexperience', required=True)
        self.requiredcontentpackages = self._getStr('requiredcontentpackages', required=True)
        self.name = self._getStr('name', required=True)
        self.previewimage = self._getStr('previewimage', required=True)
        self.items = []
        for el in data.findall('Item'):
            o = Item()
            o.deserialize(el)
            self.items += [o]
        self.gaps = []
        for el in data.findall('Gap'):
            o = Gap()
            o.deserialize(el)
            self.gaps += [o]
        self.structures = []
        for el in data.findall('Structure'):
            o = Structure()
            o.deserialize(el)
            self.structures += [o]
        self.waypoints = []
        for el in data.findall('WayPoint'):
            o = WayPoint()
            o.deserialize(el)
            self.waypoints += [o]
        self.hulls = []
        for el in data.findall('Hull'):
            o = Hull()
            o.deserialize(el)
            self.hulls += [o]

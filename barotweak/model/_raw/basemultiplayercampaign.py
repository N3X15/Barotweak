from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.bots import Bots
from barotweak.model.cargo import Cargo
from barotweak.model.map.map import Map
from barotweak.model.pendingupgrades import PendingUpgrades
from lxml import etree
import sys
from typing import List, Optional
class BaseMultiPlayerCampaign(BaseSerializable):
    TAG = 'MultiPlayerCampaign'
    def __init__(self):
        super().__init__()
        self.money: int = 0
        self.cheatsenabled: bool = False
        #self.metadata: MultiPlayerMetadata = MultiPlayerMetadata()
        self.map: Map = Map()
        self.cargo: Cargo = Cargo()
        self.pendingupgrades: PendingUpgrades = PendingUpgrades()
        self.bots: Bots = Bots()
        self.availablesubs: List[str] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['money'] = str(self.money)
        o['cheatsenabled'] = str(self.cheatsenabled)
        #o.append(self.metadata.serialize())
        o.append(self.map.serialize())
        o.append(self.cargo.serialize())
        o.append(self.pendingupgrades.serialize())
        o.append(self.bots.serialize())
        e = etree.Element('AvailableSubs')
        for value in self.availablesubs:
            inner = etree.Element('Sub')
            inner['name'] = str(value)
            e.append(inner)
        o.append(e)
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.money = self._getInt('money', required=True)
        self.cheatsenabled = self._getBool('cheatsenabled', required=True)
        #e: Optional[etree.Element] = data.find('Metadata')
        #self.metadata = None
        #if e is None:
            #print('Element <Metadata> cannot be found in element <MultiPlayerCampaign>!')
            #print('This likely means the save is corrupt. :(')
            #sys.exit(1)
        #self.metadata = MultiPlayerMetadata()
        #self.metadata.deserialize(e)
        e: Optional[etree.Element] = data.find('map')
        self.map = None
        if e is None:
            print('Element <map> cannot be found in element <MultiPlayerCampaign>!')
            print('This likely means the save is corrupt. :(')
            sys.exit(1)
        self.map = Map()
        self.map.deserialize(e)
        e: Optional[etree.Element] = data.find('cargo')
        self.cargo = None
        if e is None:
            print('Element <cargo> cannot be found in element <MultiPlayerCampaign>!')
            print('This likely means the save is corrupt. :(')
            sys.exit(1)
        self.cargo = Cargo()
        self.cargo.deserialize(e)
        e: Optional[etree.Element] = data.find('PendingUpgrades')
        self.pendingupgrades = None
        if e is None:
            print('Element <PendingUpgrades> cannot be found in element <MultiPlayerCampaign>!')
            print('This likely means the save is corrupt. :(')
            sys.exit(1)
        self.pendingupgrades = PendingUpgrades()
        self.pendingupgrades.deserialize(e)
        e: Optional[etree.Element] = data.find('bots')
        self.bots = None
        if e is None:
            print('Element <bots> cannot be found in element <MultiPlayerCampaign>!')
            print('This likely means the save is corrupt. :(')
            sys.exit(1)
        self.bots = Bots()
        self.bots.deserialize(e)
        self.availablesubs = []
        for a in data.find('AvailableSubs'):
            self.availablesubs.append(str(a.attrib['name']))

from barotweak.model._raw.basesubmarine import BaseSubmarine
class Submarine(BaseSubmarine):
    def __init__(self) -> None:
        super().__init__()

    @classmethod
    def LoadGameSession(cls, filename: str, minify: bool = False) -> 'Submarine':
        parser = etree.XMLParser(remove_blank_text=minify)
        tree = etree.parse(filename, parser=parser)
        sess = cls()
        sess.deserialize(tree.find(sess.TAG))
        return sess

    def save(self, filename: str, minify: bool = False) -> None:
        root = self.serialize()
        with open(filename, 'wb') as f:
            f.write(etree.tostring(root, xml_declaration=True, pretty_print=not minify, encoding='utf-8')

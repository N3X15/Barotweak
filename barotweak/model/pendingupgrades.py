from barotweak.model._raw.basependingupgrades import BasePendingUpgrades
from typing import Dict

class PendingUpgrades(BasePendingUpgrades):
    def __init__(self) -> None:
        super().__init__()

from barotweak.model.map._raw.basemap import BaseMap
from barotweak.model.typeddatalist import TypedDataList
from lxml import etree
class Map(BaseMap):
    def __init__(self) -> None:
        super().__init__()
        #self.metadata: TypedDataList = TypedDataList('Metadata', 'Data')
    def serialize(self) -> etree.Element:
        o = super().serialize()
        #o.append(self.metadata.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        #self.metadata.deserialize(data.find('Metadata'))

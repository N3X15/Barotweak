from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
from typing import List, Optional
class BaseMapStore(BaseSerializable):
    TAG = 'store'
    def __init__(self):
        super().__init__()
        self.balance: int = -1
        #self.stock: List[TYPE] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['balance'] = str(self.balance)
        #for e in self.stock:
            #o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.balance = self._getInt('balance', required=True)
        #self.stock = []
        #for el in data.findall('stock'):
            #o = FACTORY_METHOD()
            #o.deserialize(el)
            #self.stock += [o]

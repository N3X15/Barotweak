from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.map.mapconnection import MapConnection
from barotweak.model.map.maplocation import MapLocation
from lxml import etree
from typing import List, Optional
class BaseMap(BaseSerializable):
    TAG = 'map'
    def __init__(self):
        super().__init__()
        self.version: str = ''
        self.currentlocation: int = -1
        self.currentlocationconnection: int = -1
        self.selectedlocation: int = -1
        self.startlocation: int = -1
        self.endlocation: int = -1
        self.seed: str = ''
        self.location: List[MapLocation] = []
        self.connection: List[MapConnection] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['version'] = str(self.version)
        o['currentlocation'] = str(self.currentlocation)
        o['currentlocationconnection'] = str(self.currentlocationconnection)
        o['selectedlocation'] = str(self.selectedlocation)
        o['startlocation'] = str(self.startlocation)
        o['endlocation'] = str(self.endlocation)
        o['seed'] = str(self.seed)
        for e in self.location:
            o.append(e.serialize())
        for e in self.connection:
            o.append(e.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.version = self._getStr('version', required=True)
        self.currentlocation = self._getInt('currentlocation', required=True)
        self.currentlocationconnection = self._getInt('currentlocationconnection', required=True)
        self.selectedlocation = self._getInt('selectedlocation', required=True)
        self.startlocation = self._getInt('startlocation', required=True)
        self.endlocation = self._getInt('endlocation', required=True)
        self.seed = self._getStr('seed', required=True)
        self.location = []
        for el in data.findall('location'):
            o = MapLocation()
            o.deserialize(el)
            self.location += [o]
        self.connection = []
        for el in data.findall('connection'):
            o = MapConnection()
            o.deserialize(el)
            self.connection += [o]

from barotweak.model.baseserializable import BaseSerializable
from barotweak.model.map.maplevel import MapLevel
from barotweak.model.map.mapstore import MapStore
from lxml import etree
from typing import Any, List, Optional
class BaseMapLocation(BaseSerializable):
    TAG = 'location'
    def __init__(self):
        super().__init__()
        self.type: str = ''
        self.basename: str = ''
        self.name: str = ''
        self.discovered: bool = False
        self.position: str = ''
        self.normalizeddepth: str = ''
        self.pricemultiplier: int = -1
        self.mechanicalpricemultipler: int = -1
        self.index: int = -1
        self.changetimer: int = 0
        #self.takenitems: Optional[Any] = None
        self.killedcharacters: List[str] = []
        self.level: Optional[MapLevel] = None
        self.store: Optional[MapStore] = None
        #self.missions: Optional[MapMissions] = None
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['type'] = str(self.type)
        o['basename'] = str(self.basename)
        o['name'] = str(self.name)
        o['discovered'] = str(self.discovered)
        o['position'] = str(self.position)
        o['normalizeddepth'] = str(self.normalizeddepth)
        o['pricemultiplier'] = str(self.pricemultiplier)
        o['mechanicalpricemultipler'] = str(self.mechanicalpricemultipler)
        o['i'] = str(self.index)
        if self.changetimer != 0:
            o['changetimer'] = str(self.changetimer)
        #if self.takenitems != None:
            #o['takenitems'] = str(self.takenitems)
        if len(self.killedcharacters) > 0:
            o['killedcharacters'] = ','.join([str(x) for x in self.killedcharacters])
        if self.level is not None:
            o.append(self.level.serialize())
        if self.store is not None:
            o.append(self.store.serialize())
        #if self.missions is not None:
            #o.append(self.missions.serialize())
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.type = self._getStr('type', required=True)
        self.basename = self._getStr('basename', required=True)
        self.name = self._getStr('name', required=True)
        self.discovered = self._getBool('discovered', required=True)
        self.position = self._getStr('position', required=True)
        self.normalizeddepth = self._getStr('normalizeddepth', required=True)
        self.pricemultiplier = self._getInt('pricemultiplier', required=True)
        self.mechanicalpricemultipler = self._getInt('mechanicalpricemultipler', required=True)
        self.index = self._getInt('i', required=True)
        self.changetimer = self._getInt('changetimer', default=0, required=False)
        #self.takenitems = self._getRawValue('takenitems', default=None, required=False)
        self.killedcharacters = [str(x.strip()) for x in data.get('killedcharacters', '').split(',')]
        e: Optional[etree.Element] = data.find('Level')
        self.level = None
        if e is not None:
            self.level = MapLevel()
            self.level.deserialize(e)
        e: Optional[etree.Element] = data.find('store')
        self.store = None
        if e is not None:
            self.store = MapStore()
            self.store.deserialize(e)
        #e: Optional[etree.Element] = data.find('missions')
        #self.missions = None
        #if e is not None:
            #self.missions = MapMissions()
            #self.missions.deserialize(e)

from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
class BaseMapConnection(BaseSerializable):
    TAG = 'connection'
    def __init__(self):
        super().__init__()
        self.passed: bool = False
        self.difficulty: str = ''
        self.biome: str = ''
        self.locations: str = ''
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['passed'] = str(self.passed)
        o['difficulty'] = str(self.difficulty)
        o['biome'] = str(self.biome)
        o['locations'] = str(self.locations)
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.passed = self._getBool('passed', required=True)
        self.difficulty = self._getStr('difficulty', required=True)
        self.biome = self._getStr('biome', required=True)
        self.locations = self._getStr('locations', required=True)

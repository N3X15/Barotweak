from barotweak.model.baseserializable import BaseSerializable
from lxml import etree
from typing import List, Optional
class BaseMapLevel(BaseSerializable):
    TAG = 'Level'
    def __init__(self):
        super().__init__()
        self.seed: str = ''
        self.biome: str = ''
        self.type: str = ''
        self.difficulty: int = -1
        self.size: str = ''
        self.generationparams: str = ''
        self.eventhistory: List[str] = []
    def serialize(self) -> etree.Element:
        o=super().serialize()
        o['seed'] = str(self.seed)
        o['biome'] = str(self.biome)
        o['type'] = str(self.type)
        o['difficulty'] = str(self.difficulty)
        o['size'] = str(self.size)
        o['generationparams'] = str(self.generationparams)
        if len(self.eventhistory) > 0:
            o['eventhistory'] = ','.join([str(x) for x in self.eventhistory])
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.seed = self._getStr('seed', required=True)
        self.biome = self._getStr('biome', required=True)
        self.type = self._getStr('type', required=True)
        self.difficulty = self._getInt('difficulty', required=True)
        self.size = self._getStr('size', required=True)
        self.generationparams = self._getStr('generationparams', required=True)
        self.eventhistory = [str(x.strip()) for x in data.get('eventhistory', '').split(',')]

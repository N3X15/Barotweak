from barotweak.model.map._raw.basemaplocation import BaseMapLocation
from barotweak.model.map.mapconnection import MapConnection
from lxml import etree
class MapMission(object):
    def __init__(self, pfid: str = '', dstid: int = 0, sel: bool = False) -> None:
        self.prefabid: str = pfid
        self.destinationindex: int = dstid
        self.selected: bool = sel

class MapLocation(BaseMapLocation):
    TAG = 'missions'
    def __init__(self) -> None:
        super().__init__()
        self.missions: List[MapMission] = []

    def serialize(self) -> etree.Element:
        o=super().serialize()
        if len(self.missions) > 0:
            missions = etree.SubElement(o, 'mission', {})
            for mission in self.missions:
                etree.SubElement(missions, 'mission', {
                    'prefabid':         mission.prefabid,
                    'destinationindex': mission.destinationindex,
                    'selected':         'true' if mission.selected else 'false',
                })
        return o

    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.missions = []
        missions = data.find('missions')
        if missions is not None:
            for mission in missions.findall('mission'):
                m = MapMission()
                m.prefabid = mission.get('prefabid')
                m.destinationindex = int(mission.get('destinationindex'))
                m.selected = mission.get('destinationindex') == 'true'
                self.missions += [m]

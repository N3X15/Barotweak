from barotweak.model.map._raw.basemapstore import BaseMapStore
from lxml import etree
from typing import Dict

class MapStore(BaseMapStore):
    def __init__(self) -> None:
        super().__init__()
        self.items: Dict[str, int] = {}

    def serialize(self) -> etree.Element:
        o = super().serialize()
        for itemid, qty in self.items.items():
            etree.SubElement(o, 'item', {
                'id': itemid,
                'qty': str(qty)
            })
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        for c in data:
            if c.tag.lower() == 'item':
                self.items[c['id']] = int(c['qty'])

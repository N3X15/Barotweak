import sys
from barotweak.model.baseserializable import BaseSerializable
from typing import Any, Dict, Tuple
from enum import Enum
from lxml import etree

class MapMissions(BaseSerializable):
    def __init__(self, tag: str, entry_tag: str) -> None:
        super().__init__(tag)
        self.entry_tag: str = entry_tag
        self.key_attr: str = 'key'
        self.value_attr: str = 'value'
        self.type_attr: str = 'type'
        self.__data: Dict[str, Tuple[type, Any]] = {}

    def set(key: str, value: Any) -> None:
        cstype = self.__data[key][0]
        if (cstype == ECSType.FLOAT and not isinstance(value, float)) or (cstype == ECSType.INT and not isinstance(value, int)):
            print('Unable to set key {key!r} in TypedDataList: Wrong type.')
            sys.exit(1)
        self.__data[key][1] = value

    def get(key: str) -> Any:
        return self.__data[key][1]

    def serialize(self) -> etree.Element:
        o=super().serialize()
        for key,tval in self.__data.items():
            cstype, value = tval
            cstype = cstype.value
            etree.SubElement(o, self.entry_tag, {
                self.key_attr: key,
                self.type_attr: cstype,
                self.value_attr: str(value)
            })
        return o
    def deserialize(self, data: etree.Element) -> None:
        super().deserialize(data)
        self.__data = {}
        for el in data.findall(self.entry_tag):
            key = el[self.key_attr]
            cstype = ECSType(el[self.type_attr])
            value = self.parse(cstype, el[self.value_attr])
            self.__data[key] = (cstype, value)

    def parse(cstype: ECSType, value: str) -> Any:
        if cstype == ECSType.FLOAT:
            return float(value)
        elif cstype == ECSType.INT:
            return int(value)
        else:
            print(f'Unrecognized TypedDatum cstype {cstype!r}')
            sys.exit(1)

ALL_FIXES = {}
def DefineFix():
    def decorator(f):
        finstance = f()
        ALL_FIXES[finstance.name] = finstance
        return f
    return decorator
Fix = DefineFix()

class BaseFix(object):
    def __init__(self, name: str, desc: str) -> None:
        self.name: str = name
        self.desc: str = desc

    def register_args(self, subp) -> None:
        return

    def fix(self, args, session) -> None:
        return

import os, logging, sys
from lxml import etree

from .basefix import Fix, BaseFix

from buildtools import log

@Fix
class CheckOwnedSubs(BaseFix):
    def __init__(self) -> None:
        super().__init__('Check Owned Subs', 'Checks if you\'re using a sub name that\'s not in the availability list.')

    def register_args(self, subp) -> None:
        return

    def fix(self, args, session) -> None:
        problems: int = 0
        gamesession = session.xpath('//Gamesession')[0]
        currentsubid: str = gamesession.get('submarine')

        ownedsubselem = gamesession.find('ownedsubmarines')
        owned_subs: Lists[str] = []
        for sub in ownedsubselem.findall('sub'):
            owned_subs += [sub.get('name')]

        if currentsubid not in owned_subs:
            log.warning('Current sub %r is not in the owned subs list. Fixing...', currentsubid)
            etree.SubElement(ownedsubselem, 'sub', {name: currentsubid})
            owned_subs += [currentsubid]
            problems += 1

        available_subs: Lists[str] = []
        for sub in gamesession.find('.//AvailableSubs').findall('Sub'):
            available_subs += [sub.get('name')]

        if currentsubid not in available_subs:
            log.critical('Current sub %r is not in the server\'s available subs list. You need to rename it, or add it to the server\'s availability list, or you\'re going to get sync issues.', currentsubid)
            log.critical('See: serversettings.xml: /serversettings[@campaignsubmarines].')
            sys.exit(1)

        if problems == 0:
            log.info('OK')

import os, logging
from lxml import etree

from .basefix import Fix, BaseFix

from buildtools import log

@Fix
class FixCampaignID(BaseFix):
    '''
    I don't know why this happens, but my dedicated server occasionally decides
    that campaignid="3" or some other weird number.  Then you get errors and crashes.
    '''
    def __init__(self) -> None:
        super().__init__('Fix Campaign ID', 'Fixes erroneous campaign ID.')

    def register_args(self, subp) -> None:
        subp.add_argument('--campaign-id', type=int, default=1, help='What to reset the campaign ID to. Starts at 1.')

    def fix(self, args, session) -> None:
        gamesession = session.xpath('//Gamesession')[0]
        campaignID = gamesession.get('campaignid')
        if int(campaignID) != args.campaign_id:
            log.warning('Incorrectly set to %r, changed to %d.', campaignID, args.campaign_id)
            gamesession.attrib['campaignid'] = str(args.campaign_id)
        else:
            log.info('No problems')

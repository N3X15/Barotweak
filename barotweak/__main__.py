import sys
import gzip
import struct
import os

from buildtools import os_utils, log
from .commands import register_commands
from .io import compressFilesIn, decompressFilesIn

def main():
    import argparse
    argp = argparse.ArgumentParser()
    subp = argp.add_subparsers()

    register_commands(subp)

    args = argp.parse_args()
    if args.command is None:
        argp.print_help()
    else:
        args.command(args)
main()
